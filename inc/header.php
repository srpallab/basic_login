<?php session_start(); ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <title>Basic Login</title>
    <link href="css/normalize.css" rel="stylesheet"/>
    <link href="css/milligram.css" rel="stylesheet"/>
    <link href="css/main.css" rel="stylesheet"/>
  </head>
  <body>
    <header class="container">
      <nav class="row row-center">
	<a class="column column-10" href="#">LOGO</a>
	<a class="column column-10" href="index.php">Home</a>
	<a class="column column-10" href="#">Protfolio</a>
	<a class="column column-10" href="#">About Me</a>
	<a class="column column-10" href="#">Contact</a>
	<?php if(!isset($_SESSION['userId'])): ?>
	  <form class="column inline" action="inc/login.inc.php" method="post">
            <input class="column inline column-33"
		   name="mailuid" type="text" placeholder="UserName or Email"/>
            <input class="column inline column-33"
		   name="password" type="password" placeholder="Password"/>
	    <button type="submit" name="login-submit">Sign In</button>
	  </form>
	  <a class="column column-10" href="signup.php">Sign Up</a>
	<?php else: ?>
	  <form class="column inline column-10"
		action="inc/logout.inc.php" method="post">
            <button type="submit" name="logout-submit">Sign Out</button>
	  </form>

	<?php endif; ?>
	
      </nav>
    </header>
    
