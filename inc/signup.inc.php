<?php
if (isset($_POST['signup-submit'])) {
    require 'dbh.inc.php';
    $uid = $_POST['uid'];
    $email = $_POST['email'];
    $pwd = $_POST['pwd'];
    $pwdRe = $_POST['pwd-repeat'];
    if(empty($uid) || empty($email) || empty($pwd) || empty($pwdRe)){
        $url = sprintf("uid=%s&email=%s",$uid, $email);
        header("Location: ../signup.php?error=emptyfield&".$url);
        exit();
    }elseif (
        !filter_var($email, FILTER_VALIDATE_EMAIL) &&
        !preg_match("/^[a-zA-Z0-9]*$/", $uid)
    ) {
        header("Location: ../signup.php?error=invalidemailuid");
        exit();
    }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $url = sprintf("uid=%s",$uid);
        header("Location: ../signup.php?error=invalidemail&".$url);
        exit();
    }elseif (!preg_match("/^[a-zA-Z0-9]*$/", $uid)) {
        header("Location: ../signup.php?error=invaliduid&email=".$email);
        exit();
    }elseif ($pwd !== $pwdRe) {
        $url = sprintf("uid=%s&email=%s",$uid, $email);
        header("Location: ../signup.php?error=passwordcheck&".$url);
        exit();
    }else{
        $sql = "SELECT uidUsers FROM users WHERE uidusers=? ";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../signup.php?error=sqlerror");
            exit();
        }else{
            mysqli_stmt_bind_param($stmt, "s", $uid);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            $resultCheck = mysqli_stmt_num_rows($stmt);
            if ($resultCheck > 0) {
               header("Location: ../signup.php?error=uidtaken&email=".$email);
               exit(); 
            }else{
                $sql = "INSERT INTO users(uidusers, emailUsers, pwdUsers) VALUES(?, ?, ?)";
                $stmt = mysqli_stmt_init($conn);
                if (!mysqli_stmt_prepare($stmt, $sql)) {
                    header("Location: ../signup.php?error=sqlerror");
                    exit();
                }else{
                    $pwdHashed = password_hash($pwd, PASSWORD_DEFAULT);
                    mysqli_stmt_bind_param($stmt, "sss", $uid, $email, $pwdHashed);
                    mysqli_stmt_execute($stmt);
                    header("Location: ../signup.php?signup=success");
                    exit();
                }
            }
        }
    }
    mysqli_stmt_close($stmt);
    mysqli_close();
}else{
    header("Location: ../signup.php");
    exit();
}
