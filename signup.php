<?php include_once 'inc/header.php' ?>
<main class="container">
  <h1>Sign Up</h1>
  <form method="post" action="inc/signup.inc.php">
    <input name="uid" type="text" placeholder="Username" required/>
    <input name="email" type="email" placeholder="Email" required/>
    <input name="pwd" type="password" placeholder="Password" required/>
    <input name="pwd-repeat"
	   type="password" placeholder="Repeat Password" required/>
    <button type="submit" name="signup-submit">SingUp</button>
  </form>
</main>
<?php include_once 'inc/footer.php' ?>
